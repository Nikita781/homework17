#include <iostream>
#include <math.h>

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << x << " " << y << " " << z << "\n";
    }

    void VectorModulus()
    {
        double VectorModulus;
        VectorModulus = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << VectorModulus << "\n";
    }

private:
    double x;
    double y;
    double z;
};


int main()
{
    Vector vector(0,4,3);
    vector.Show();
    vector.VectorModulus();
}

